classdef cwtprovider
    %CACHE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
       wave_name = 'morl'; %Type of 1-d continuous wavelet
       Bo = 1.701;%Bandwidth of mother Morlet wavelet, in radians
       omega_0 = 5;%Center frequency of mother Morlet wavelet, in radians
       BCF;%Center Frequency
       BS;%Bandwidth at scale specific scale (cLevels)
       LBL;%Lowermost Frequency at scale
       fsample;
       cLevels;
    end
    
    methods
        function obj = cwtprovider(fsample,cLevels)
            obj.fsample=fsample;
            obj.cLevels=cLevels;    
            Nu_0 = obj.omega_0 / (2 * pi);
            obj.BCF = Nu_0 ./ cLevels * fsample;            
            obj.BS = obj.Bo ./ (4 * pi * cLevels) * fsample;    
            obj.LBL = obj.BCF - obj.BS ./ 2;                          
        end
        
        function value = provide(obj,signal)
            value = cwt(signal, obj.cLevels, obj.wave_name);  
        end
           
    end
end
