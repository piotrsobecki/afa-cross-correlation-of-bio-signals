% Program RunECGBPScalogram.m
% Command: RunECGBPScalogram
% Objective: Obtain the scalogram and coscalogram of ECG signal and BP data
% And examine the relationship between the two processes.
% Programmer: Bonnie Jonkman
% Date: 9 December 2004
% Modified by Arati Gurung for EE 5667 Project
% Date: 6 December 2010

clear all;

out_file = input('Enter file name:','s'); %'ecgbp.txt';
tmin = input('Enter minimum time to analyze (seconds):');%2;          minimum time to analyze
tmax = input('Enter maximum time to analyze (seconds):'); %100;        % maximum time to analyze
fsample = input('Enter desired sample frequency:');%500;    % Desired number of samples per second (desired sampling rate)
cLevels = input('Enter scales of continuous analysis (1:10:400):');%1:10:400; %Scales of continuous analysis
plot_columns  =  input('Enter the three columns to be used:'); %[1 3 7]; % ECG (mV) and BP (mmHg)
data_title1= input('Enter name of the first data set:','s');% 'ECG';
data_title2= input('Enter name of the second data set:','s');%'BP';
data_units1= input('Enter units for the first data set:','s');%'mV';
data_units2= input('Enter units for the second data set:','s');%'mmHg';
data_units3= input('Enter shared units for both data sets:','s');%'seconds';
plot_title = input('Enter the title for the plot:','s'); %'Coscalogram of ECG and Arterial Blood Pressure';
bandwidth = input('Enter the bandwidth (rad/s):');
centerFreq = input('Enter the center frequency (rad/s):');



[loadData, time] = loadColumnData(out_file, plot_columns,'\t'); 
numCols  = size(loadData,2);%number of columns
load     = loadData(:,numCols);%get all rows of 5th column

input_scalogram2(time, loadData(:,numCols-1), load, plot_title, data_title1,...
   data_title2,data_units1, data_units2, data_units3, tmin, tmax, fsample, cLevels, bandwidth,centerFreq); 

input_scalogram3d2(time, loadData(:,numCols-1), load, plot_title, data_title1,...
   data_title2,data_units1, data_units2, data_units3, tmin, tmax, fsample, cLevels, bandwidth,centerFreq);

% end RunECGBPScalogram