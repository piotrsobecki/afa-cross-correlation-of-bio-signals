% Program scalogram2.m
% Programmer: Bonnie Jonkman
% Date: 17 August 2004
% Purpose: Analysis of turbine response with scalograms of two signals (y1
% & y2) and their coscalogram.
function [dLevels, lvl ] = scalogram2(time, y1, y2, plot_title, y1_title,y2_title, y1_units,y2_units,t_units, tmin, tmax, fsample, cLevels)

    
setYaxis(1:5) = false;

if nargin > 13
    len = length(yMIN);
    setYaxis(1:len) = isfinite(yMIN);
end

FntSz = 8;

discard = true;         %whether or not to discard scales less than the Nyquist
byscale = true;        %True == Color coefficients by each scale (not "all" scales)

Bo = 1.701;             %Bandwidth of mother Morlet wavelet, in radians
omega_0 = 5;            %Center frequency of mother Morlet wavelet, in radians

%for Morlet Wavelet
Nu_0 = omega_0 / (2 * pi);
BCF = Nu_0 ./ cLevels * fsample;            %Center Frequency
Bs = Bo ./ (4 * pi * cLevels) * fsample;    %Bandwidth at scale specific scale (cLevels)
LBL = BCF - Bs./2;                          %Lowermost Frequency at scale
UBL = BCF + Bs./2;                          %Uppermost Frequency at scale

%Optimize
minDelay=0.5;
maxDelay=4.5;% maximum considered delay in seconds
[c1,c2,shifts] = matchtimedelay_forfrequency(y1,y2,time, fsample, cLevels,minDelay,maxDelay);


tmin1 = time(1);                %min time value in event
tmax1 = time(length(time));     %max time value in event (l.u.b. of max time & NaN for level 9)

%Linearly Interpolate Loads so that wavelet detail is better seen
time_interp = [ tmin1:1/fsample:tmax1 ]';

% chop off the ends that we don't want to see...
tmin = max( [ tmin, time(1) ] );                %min time value in event
tmax = min( [ tmax, time(length(time)) ] );     %max time value in event (l.u.b. of max time & NaN for level 9)

mask        = find(time_interp>=tmin & time_interp<=tmax);
time_interp = time_interp(mask);

c1          = c1(:,mask);
c2          = c2(:,mask);

%scalograms
%coscg = c1 .* c2;
%c1    = c1.^2;
%c2    = c2.^2;

%----------------------------------------------------------------
%Plot the results
%----------------------------------------------------------------
figure;
leftMargin = tmin+(tmax-tmin)*.05;       % the leftmost value of text printed on each plot
title( plot_title, 'Interpreter','none' );
%----ScaloGrams----------------
color_levels = 128;
sz = size(c1);

for i_fig = 1:3

    h=subplot(3,1,i_fig);
    set(h,'FontSize', FntSz);
    colormap( jet(color_levels) );
    if (i_fig == 1)
        im = c1.^2; %scalogram
        subplot_title = {y1_title; 'Scalogram'; 'Scale'} ;
    elseif (i_fig == 2)
        im = c2.^2; %scalogram
        subplot_title = {y2_title; 'Scalogram'; 'Scale'};
    else
        im = c1.*c2;  %coscalogram
        subplot_title =  {[deblank(y1_title) '-' deblank(y2_title)]; 'Coscalogram'; 'Scale'};
    end
   
    if setYaxis(i_fig+2)
        im = codeMatrixImage(im,color_levels,yMIN(i_fig+2),yMAX(i_fig+2));
    else
        if (byscale == true)
            im = wcodemat(im,color_levels,'row',1);
        else %by all scales
            im = wcodemat(im,color_levels,'mat',1);
        end
    end
    
    image(time_interp, cLevels, im);
    ylabel(subplot_title);

    h = gca;
    set(h,'YDir','reverse','Box','off');      
    xlim([tmin, tmax]);
    
    if i_fig < 3
        set(h,'XTickLabel',{[]})        
    else
        xlabel(['Time ' t_units ] );
    end
    
    %------ add a second axis for cLevel labels on the right of the plot ---------
    ax2 = axes('Position',get(h,'Position'),...
               'XAxisLocation','top',...
               'YAxisLocation','right',...
               'Color','none',...
               'XColor','k','YColor','k',...
               'XTickLabel',{[]},...
               'YDir','reverse',...
               'YLim',get(h,'YLim'), 'XLim', get(h,'XLim'),...
               'FontSize',FntSz-1);
    ylabel('Frequency','FontSize',FntSz-1); 
   
    
    %----------- calculate the non-linear y-ticks (necessary because of the  -----%
    %----------- way the image() function uses the x and y axes to determine -----%
    %----------- the location of pixels)                                     -----%
    len = length(cLevels);
    dy = max([1, floor(len/10)]); %minimum distance between indices

    k = 1;
    tBCF = round(BCF*10)./10;

    ylab = [];
    ylab2 = [];

    shiftForLevel = -1 * shifts(k)/fsample;
    labelForFirst = strcat(num2str(tBCF(k),'%4.1f'),'(',num2str(shiftForLevel),'s)');
    ylab  = [ylab; cLevels(k)]; %start at bottom (smallest level == largest frequency);
    ylab2 = [ylab2; {labelForFirst}];
    
    next_k   = k + dy;    %next possible index

    t = tBCF(1); %largest frequency....
    p = 2;
    nextLab = min([find(tBCF<t), len+1]); %next index with frequency value smaller than last one

    while nextLab <= len
        if nextLab >= next_k
            k = nextLab;
            shiftForLevel = -1 * shifts(k)/fsample;
            labelForP =  strcat(num2str(tBCF(k),'%4.1f'),'(',num2str(shiftForLevel),'s)');
            ylab = [ylab;  cLevels(k)];
            ylab2 = [ylab2; {labelForP} ];
   
            p = p + 1;
            next_k = k + dy;
        end
        t = tBCF(nextLab);
        nextLab = min([find(tBCF<t), len+1]); %next index with value greater than last one
    end

    set(ax2,'YTick', ylab,'YTickLabel',ylab2);     
    

end %i_fig


