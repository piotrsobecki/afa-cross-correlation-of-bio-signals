% Program RunECGBPScalogram.m
% Command: RunECGBPScalogram
% Objective: Obtain the scalogram and coscalogram of ECG signal and BP data
% And examine the relationship between the two processes.
% Programmer: Bonnie Jonkman
% Date: 9 December 2004
% Modified by Arati Gurung for EE 5667 Project
% Date: 6 December 2010

tmin = 5;         % minimum time to analyze
tmax = 85;        % maximum time to analyze

%tmin = 30;         % minimum time to analyze
%tmax = 50;        % maximum time to analyze
fsample = 500;    % Desired number of samples per second (desired sampling rate)
cLevels = 1:10:400; %Scales of continuous analysis
plot_columns  = [1,3,7]; % ECG (mV) and BP (mmHg)
plot_title = 'Coscalogram of ECG and Arterial Blood Pressure.';
out_file = 'ecgbp.txt';
[loadData, time] = loadColumnData(out_file, plot_columns,'\t'); 
numCols  = size(loadData,2);%number of columns
load     = loadData(:,numCols);%get all rows of 5th column
data_title1='ECG';
data_title2='BP';
data_units1='mV';
data_units2='mmHg';
data_units3='seconds';

y1 = loadData(:,numCols-1); % first 
y2 = load; %second

%Optimize
minDelay=0.5;
maxDelay=4.5; % Maximum considered delay in seconds
[y1,y2,delay] = matchtimedelay(...
    y1,... % First input signal
    y2,... % Second input signal
    time,... % Array representing time domain
    fsample,... % Sampling frequency
    cLevels,... % Scales for continuous wavelet transform analysis
    minDelay,... % Minimum delay in seconds
    maxDelay... % Maximum delay in seconds
);

%plot_title = strcat(plot_title, ' Delay equal to (', num2str(delay),' seconds).');

%After optimization
scalogram2(time, y1, y2, plot_title, data_title1,data_title2,data_units1, data_units2, data_units3, tmin, tmax, fsample, cLevels); 
scalogram3d2(time, y1, y2, plot_title, data_title1,data_title2,data_units1, data_units2, data_units3, tmin, tmax, fsample, cLevels);
% end RunECGBPScalogram