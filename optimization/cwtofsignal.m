% Program scalogram2.m
% Programmer: Bonnie Jonkman
% Date: 17 August 2004
% Purpose: Analysis of turbine response with scalograms of two signals (y1
% & y2) and their coscalogram.
function [c1,time_interp] = cwtofsignal(time, y1, fsample, cLevels, time_interp)

discard = true;         %whether or not to discard scales less than the Nyquist

%------Continuous Wavelet Analysis Variables------
wave_name2 = 'morl';    %Type of 1-d continuous wavelet
Bo = 1.701;             %Bandwidth of mother Morlet wavelet, in radians
omega_0 = 5;            %Center frequency of mother Morlet wavelet, in radians

%for Morlet Wavelet
Nu_0 = omega_0 / (2 * pi);
BCF = Nu_0 ./ cLevels * fsample;            %Center Frequency
Bs = Bo ./ (4 * pi * cLevels) * fsample;    %Bandwidth at scale specific scale (cLevels)
LBL = BCF - Bs./2;                          %Lowermost Frequency at scale

if (discard)
    Nyquist = 1/( (time(2)-time(1)) * 2);

    ix = (LBL<=Nyquist);      %Discard frequencies greater than Nyquist
    cLevels = cLevels(ix);
end

%Continuous wavelet transform
c1 = cwt(y1, cLevels, wave_name2);
