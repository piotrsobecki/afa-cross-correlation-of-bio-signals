function [x,y,bestFitness,bestShift] = fitnessplot(counter,from,to,fitnessFunction)
     x = zeros(abs(to-from+1),1);
     y = zeros(abs(to-from+1),1);
     i=0;
     bestFitness=inf;
     bestShift=0;
       for testShift = from:1:to
           i=i+1;
           fit =fitnessFunction(testShift); 
           x(i)=testShift;
           y(i)=fit;
           if fit<bestFitness
               bestFitness = fit;
               bestShift = testShift;
           end
       end
     figure 
     pl = plot(x,y);
     saveas(pl, strcat('fitnessplot',num2str(counter),'.png'),'png');
end