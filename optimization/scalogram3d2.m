% Program scalogram3d2.m
% Programmer: Bonnie Jonkman
% Date: November 2004
% Purpose: Analysis of turbine response
function [dLevels, lvl ] = scalogram3d2(time, y1, y2, plot_title, y1_title,y2_title, y1_units,y2_units,t_units, tmin, tmax, fsample, cLevels)

setYaxis(1:5) = false;

if nargin > 13
    len = length(yMIN);
    setYaxis(1:len) = isfinite(yMIN); %this just sets the z-axis scale so that we can compare different plots easily
end

FntSz = 8;

discard = true;         %whether or not to discard scales less than the Nyquist
byscale = true;         %True == Color coefficients by each scale (not "all" scales)

%------Continuous Wavelet Analysis Variables------
wave_name2 = 'morl';    %Type of 1-d continuous wavelet
Bo = 1.701;             %Bandwidth of mother Morlet wavelet, in radians
omega_0 = 5;            %Center frequency of mother Morlet wavelet, in radians

%for Morlet Wavelet
Nu_0 = omega_0 / (2 * pi);
BCF = Nu_0 ./ cLevels * fsample;            %Center Frequency
Bs = Bo ./ (4 * pi * cLevels) * fsample;    %Bandwidth at scale specific scale (cLevels)
LBL = BCF - Bs./2;                          %Lowermost Frequency at scale
UBL = BCF + Bs./2;                          %Uppermost Frequency at scale

if (discard)
    Nyquist = 1/( (time(2)-time(1)) * 2);

    ix = (LBL<=Nyquist);      %Discard frequencies greater than Nyquist
    cLevels = cLevels(ix);
    BCF = BCF(ix);
    LBL = LBL(ix);
    UBL = UBL(ix);
end

lvl = [BCF(1), BCF(length(BCF))];

tmin1 = time(1);                %min time value in event
tmax1 = time(length(time));     %max time value in event (l.u.b. of max time & NaN for level 9)


%Linearly Interpolate Loads so that wavelet detail is better seen
time_interp = [ tmin1:1/fsample:tmax1 ]';
y1 = interp1(time, y1, time_interp, 'linear');
y2 = interp1(time, y2, time_interp, 'linear');

%Continuous wavelet transform
c1 = cwt(y1, cLevels, wave_name2);
c2 = cwt(y2, cLevels, wave_name2);

% chop off the ends that we don't want to see...
tmin = max( [ tmin, time(1) ] );                %min time value in event
tmax = min( [ tmax, time(length(time)) ] );     %max time value in event (l.u.b. of max time & NaN for level 9)

mask        = find(time_interp>=tmin & time_interp<=tmax);
time_interp = time_interp(mask);
c1          = c1(:,mask);
c2          = c2(:,mask);
y1          = y1(mask);
y2          = y2(mask);

%c1 = fft(c1);
%c2 = fft(c2);



%scalograms
%coscg = c1 .* c2;
%c1    = c1.^2;
%c2    = c2.^2;

%----------------------------------------------------------------
%Plot the results
%----------------------------------------------------------------
figure;
set(gcf,'renderer','zbuffer');
%leftMargin = tmin+(tmax-tmin)*.05;       % the leftmost value of text printed on each plot

%----Signal---------------------------------------------
h=subplot(11,1,[1:2]);
set(h,'FontSize', FntSz);

[ax, h1, h2] = plotyy(time_interp,y1, time_interp, y2);
%set(ax(1),'XTickLabel',{[]}, 'FontSize', FntSz, 'xlim', [tmin, tmax], 'YColor', 'red');
set(ax(1),                   'FontSize', FntSz, 'xlim', [tmin, tmax], 'YColor', 'red');
set(ax(2),'XTickLabel',{[]}, 'FontSize', FntSz, 'xlim', [tmin, tmax], 'YColor', 'blue');
set(h1, 'LineStyle','-', 'Color', 'red'  );
set(h2, 'LineStyle','-', 'Color', 'blue' );
set(get(ax(1),'ylabel'),'string',{y1_title; y1_units}, 'FontSize', FntSz);
set(get(ax(2),'ylabel'),'string',{y2_title; y2_units}, 'FontSize', FntSz);
set(get(ax(1),'xlabel'),'string', ['Time ' t_units], 'FontSize', FntSz);

title( plot_title, 'Interpreter','none' );

for i=1:2
    if setYaxis(i)
        set(ax(i),'ylim',[yMIN(i), yMAX(i)]);
    end
end

%text(leftMargin, ymin+0.85*(ymax-ymin), [load_title],'FontSize',FntSz,'Color','Black');

%----ScaloGrams----------------
color_levels = 128;
sz = size(c1);

for i_fig = 3:3 %1:3

    h=subplot(11,1,[3:11]);
    set(h,'FontSize', FntSz);
    colormap( jet(color_levels) );
    
    if (i_fig == 1)
        im = c1.^2; %scalogram
        subplot_title = {y1_title; 'scalogram'} ;
    elseif (i_fig == 2)
        im = c2.^2; %scalogram
        subplot_title = {y2_title; 'scalogram'};
    else
        im = abs(c1.*c2);  %coscalogram
        %im = c1.*c2;
        subplot_title =  {[deblank(y1_title) '-' deblank(y2_title)]; 'Coscalogram'};
    end
   
    
    if (byscale == true)
        C = wcodemat(im,color_levels,'row',1);
    else %by all scales
        C = wcodemat(im,color_levels,'mat',1);
    end
    
    %mesh(time_interp, cLevels, im, C);
    mesh(time_interp, BCF, im, C);
    zlabel(subplot_title);
    ylabel('Center Frequencies (Hz)');

    h = gca;
    ylab = [0.5 0.8 1:10 20];
    set(h,'YDir','reverse','Box','off',...
        'YScale','log','YTick', ylab,...
          'ylim',[min(BCF) max(BCF)], 'YMinorGrid','off', 'YMinorTick','off'  );      
    
    xlim([tmin, tmax]);
    if setYaxis(i_fig+2)
        set(h,'zlim',[yMIN(i_fig+2), yMAX(i_fig+2)]);
    end
    
    xlabel(['Time ' t_units]);
   

    
end %i_fig


